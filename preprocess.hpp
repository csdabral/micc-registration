#ifndef PREPROCESS_HPP_INCLUDED
#define PREPROCESS_HPP_INCLUDED

#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

#include <CGAL/enum.h>
//#include <CGAL/Plane_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Point_3.h>
#include <CGAL/value_type_traits.h>
#include <CGAL/property_map.h>
#include <boost/property_map/property_map.hpp>

#include "debug.hpp"

template <class>
struct range_t;

/**
* Cull all points on the "negative" side of any
* of the given planes. The returned vector is a shallow
* copy of the passed vector, with points culled.
**/

template <class Point_3, class Plane_3>
std::vector<Point_3>* cull(const std::vector<Point_3>& p, const std::vector<Plane_3> bounding_region)
{
    std::vector<Point_3>* culled = new std::vector<Point_3>();
    for (typename std::vector<Point_3>::const_iterator i = p.cbegin(); i != p.cend(); i++)
    {
        DEBUG("cull() Testing " << (*i));
        bool isInside = true;
        for (typename std::vector<Plane_3>::const_iterator j = bounding_region.cbegin(); j != bounding_region.cend(); j++)
        {
            isInside = (isInside) & (!(j->has_on_negative_side(*i)));
            DEBUG("cull() Plane " << (*j) << " " << isInside << " " << (!(j->has_on_negative_side(*i))));
        }
        if (isInside)
        {
            DEBUG("isInside");
            culled->push_back(*i);
        }
    }

    return culled;
}

template <class Points_3_Map_Out, typename OutputIterator_Points, class Point_3_Map_In, class InputIterator_Points, class InputIterator_Planes>
void cull(InputIterator_Points p_begin, InputIterator_Points p_beyond, Point_3_Map_In in_map,
          InputIterator_Planes planes_begin, InputIterator_Planes planes_beyond,
          OutputIterator_Points out, Points_3_Map_Out out_map)
{
	typedef typename CGAL::value_type_traits<OutputIterator_Points>::type EP;
//	std::cout << "foo " << typeid(EP).name() << std::endl;
	for (InputIterator_Points i = p_begin; i != p_beyond; i++)
    {
        DEBUG("cull() Testing " << get(in_map, *i));
        bool isInside = true;
        for (InputIterator_Planes j = planes_begin; j != planes_beyond; j++)
        {
        	isInside = (isInside) & (!(j->has_on_negative_side(get(in_map, *i))));
        }
        if (isInside)
        {
        	EP p;
        	put(out_map, p, get(in_map, *i));
        	*out++ = p;
        }
    }
}

template <class Point_3, class Vector_3>
std::vector<Point_3>* translate(const std::vector<Point_3>& p, const Vector_3& v)
{
    std::vector<Point_3>* t = new std::vector<Point_3>();
    t->reserve(p.size());
    std::transform(p.cbegin(), p.cend(), std::back_inserter(*t), [v](Point_3 point){return point + v;});
    return t;
}

template <class Point_Map_In, typename InputIterator, class Point_Map_Out, typename OutputIterator, class Vector>
void translate(Point_Map_In in_map, InputIterator p_begin, InputIterator p_beyond,
		Point_Map_Out out_map, OutputIterator out, const Vector& v)
{
	typedef typename CGAL::value_type_traits<OutputIterator>::type EP;
	std::for_each(p_begin, p_beyond,
			[v, in_map, out, out_map](typename CGAL::value_type_traits<InputIterator>::type p)
			mutable {EP pt; put(out_map, pt, get(in_map, p) + v); *out++ = pt;});
}

template <typename Iterator, typename Point_Map>
range_t<typename boost::property_traits<Point_Map>::value_type> range(Iterator begin, Iterator end, Point_Map pmap)
{
	typedef typename boost::property_traits<Point_Map>::value_type Point;
	typedef typename CGAL::value_type_traits<Iterator>::type EnrichedPoint;
	range_t<Point> range;

    EnrichedPoint min_x = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.x() < b.x()) return ep1; return ep2; });
    range.min_x = get(pmap, min_x);

    EnrichedPoint min_y = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.y() < b.y()) return ep1; return ep2; });
    range.min_y = get(pmap, min_y);

    EnrichedPoint min_z = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.z() < b.z()) return ep1; return ep2; });
    range.min_z = get(pmap, min_z);

    EnrichedPoint max_x = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.x() > b.x()) return ep1; return ep2; });
    range.max_x = get(pmap, max_x);

    EnrichedPoint max_y = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.y() > b.y()) return ep1; return ep2; });
    range.max_y = get(pmap, max_y);

    EnrichedPoint max_z = std::accumulate(begin, end, *begin,
    		[pmap](EnrichedPoint ep1, EnrichedPoint ep2){ Point a = get(pmap, ep1); Point b = get(pmap, ep2); if (a.z() > b.z()) return ep1; return ep2; });
    range.max_z = get(pmap, max_z);

    return range;
}

template <class Point_3>
range_t<Point_3> range(const std::vector<Point_3>& p)
{
    range_t<Point_3> range;
    range.min_x = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.x() < b.x()) return a; return b; });
    range.min_y = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.y() < b.y()) return a; return b; });
    range.min_z = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.z() < b.z()) return a; return b; });
    range.max_x = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.x() > b.x()) return a; return b; });
    range.max_y = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.y() > b.y()) return a; return b; });
    range.max_z = std::accumulate(p.cbegin(), p.cend(), p[0], [](Point_3 a, Point_3 b){ if (a.z() > b.z()) return a; return b; });
    return range;
}

template <class Point_3>
struct range_t
{
    Point_3 min_x;
    Point_3 min_y;
    Point_3 min_z;
    Point_3 max_x;
    Point_3 max_y;
    Point_3 max_z;
};

template <class Point_3>
std::ostream& insert(std::ostream& os, const range_t<Point_3>& r)
{
    return os << "min_x: " << r.min_x.x() << ", min_y: " << r.min_y.y() << ", min_z: " << r.min_z.z()
        << ", max_x: " << r.max_x.x() << ", max_y: " << r.max_y.y() << ", max_z: " << r.max_z.z();
}

template <class Point_3>
std::ostream& operator <<(std::ostream& os, const range_t<Point_3>& r)
{
    return insert(os, r);
}

#endif // PREPROCESS_HPP_INCLUDED
