#ifndef DEBUG_HPP_INCLUDED
#define DEBUG_HPP_INCLUDED

#include <iostream>

#define DEBUGGING_ENABLED (true)
#undef DEBUGGING_ENABLED

#ifdef DEBUGGING_ENABLED
#   define DEBUG(x) do { std::cerr << x << std::endl; } while (0)
#else
#   define DEBUG(x) do {} while(0)
#endif // DEBUGGING_ENABLED

#endif // DEBUG_HPP_INCLUDED
