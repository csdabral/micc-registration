/*
 * Main_includes.hpp
 *
 *  Created on: 17-May-2015
 *      Author: chinmaya
 */
#ifndef SOURCE_MAIN_INCLUDES_HPP_
#define SOURCE_MAIN_INCLUDES_HPP_

// standard libraries
#include <iostream>
#include <vector>
#include <iterator>
#include <fstream>
#include <algorithm>
#include <string>
#include <tuple>
#include <cmath>
#include <limits>

//#include <boost/property_map/compose_property_map.hpp>
//#include <boost/property_map/property_map.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/property_map/function_property_map.hpp>

// This is only for Eclipse parser; should be done automatically otherwise
//#define CGAL_EIGEN3_ENABLED

// CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/Plane_3.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/Kernel/global_functions_2.h>
#include <CGAL/Kernel/global_functions_3.h>
#include <CGAL/Aff_transformation_3.h>
#include <CGAL/property_map.h>
#include <CGAL/IO/read_xyz_points.h>
#include <CGAL/IO/write_xyz_points.h>
//#include <CGAL/IO/Polyhedron_iostream.h>
//#include <CGAL/IO/scan_OFF.h>
#include <CGAL/jet_estimate_normals.h>
#include <CGAL/mst_orient_normals.h>
#include <CGAL/jet_smooth_point_set.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Segment_3.h>
#include <CGAL/circulator.h>
#include <CGAL/In_place_list.h>
#include <CGAL/Direction_3.h>
#include <CGAL/intersections.h>
//#include <CGAL/bounding_box.h>
#include <CGAL/grid_simplify_point_set.h>

// project
#include "Modified_polyhedron_scan_OFF.h"
#include "preprocess.hpp"
#include "debug.hpp"
#include "utilities.hpp"
//#include "contour.hpp"
#include "projection.hpp"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

// Explicit instantiations; should improve compile time when precompiling header
template class CGAL::Polyhedron_3<K>;
template class Cell<K>;
template CGAL::Direction_3<K> get_old_x_direction<K>(double);
template CGAL::Aff_transformation_3<K> get_axis_change_transformation<K, CGAL::Direction_3<K> >(CGAL::Direction_3<K>, CGAL::Direction_3<K>);
template std::vector<Cell<K>*> make_grid<K>(CGAL::Point_3<K>, double, double, int, int, int);
template Cell<K>* find_root<K>(Cell<K>*);
template void assign_facets_to_cells<K>(CGAL::Polyhedron_3<K>&, Cell<K>*);
template bool is_point_triangle_vertex<K::Point_3, K::Triangle_3>(K::Point_3, K::Triangle_3);
template bool is_visible<K, K::Point_3>(K::Point_3, Cell<K>*);

#endif /* SOURCE_MAIN_INCLUDES_HPP_ */
